package TheInternet.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.time.LocalTime;

public class ReportTest {

    public static void main(String[] args) {

        ExtentHtmlReporter reporter = new ExtentHtmlReporter("src//main//resources//reports//" + LocalTime.now().getNano() + ".html");
        ExtentReports reports = new ExtentReports();
        reports.attachReporter(reporter);



    }


}
