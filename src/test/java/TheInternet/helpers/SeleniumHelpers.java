package TheInternet.helpers;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalTime;

public class SeleniumHelpers {

    public static void takeScreenShot (WebDriver driver) throws IOException {
        TakesScreenshot takesScreenShot = (TakesScreenshot) driver;
        File screeshotFile = takesScreenShot.getScreenshotAs(OutputType.FILE);
        File destinationFile = new File ("src//main//resources//ScreenShots//" + LocalTime.now().getNano() + ".png");
        Files.copy(screeshotFile.toPath(),destinationFile.toPath());
    }

}
