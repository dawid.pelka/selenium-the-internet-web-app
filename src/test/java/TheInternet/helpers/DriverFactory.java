package TheInternet.helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

    private static WebDriver driverInstance;

    public static WebDriver getDriver() {
        if (driverInstance == null) {
            String driverPath = "C:\\Users\\Wa-Dawid P\\IdeaProjects\\TheInternetWebApp\\src\\main\\resources\\executables\\drivers\\chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", driverPath);
            driverInstance = new ChromeDriver();
            driverInstance.manage().window().maximize();
            driverInstance.close();
        }
        return driverInstance;
    }
}

