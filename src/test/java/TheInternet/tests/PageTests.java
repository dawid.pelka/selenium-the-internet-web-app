package TheInternet.tests;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import TheInternet.helpers.SeleniumHelpers;
import TheInternet.helpers.TestListener;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Listeners(TestListener.class)

public class PageTests extends BaseSeleniumTest {
    

    @Test(priority = 0) // priority ustala kolejność odpalania testów. (0 - 999999)
    public void addRemovElements () throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this); //PageFactory jest potrzebny żeby FindBy mogło działać, bez tego nie ruszy
        ExtentTest test = reports.createTest("addRemovElements");//dodawanie raportu do testu
        driver.get("http://the-internet.herokuapp.com/");
        AddRemoveElementsButton.click();
        test.info("After click link on home page");   //W raporcie wyświetla nam ta informacje
        AddElementButton.click();
        test.info("After click on button");   //W raporcie wyświetla nam ta informacje
        System.out.println("Added 'Delete' button");
        Thread.sleep(1000);
        DeleteButton.click();
        System.out.println("Removed 'Delete' button");
        Thread.sleep(1000);
        driver.close();
    }

    @Test(priority = 1 )
    public void checkboxes () throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
        reports.createTest("checkboxes");
        driver.get("http://the-internet.herokuapp.com/");
        checkboxesButton.click();
        WebElement checkbox1 = driver.findElement(By.xpath("//input[@type='checkbox']"));
        if(checkbox1.isSelected()) {
            System.out.println("Checkbox is selected");
            checkbox1.click();
            System.out.println("Checkbox został odznaczony");
        } else {
            System.out.println("Checkbox is not selected");
        }
        Thread.sleep(1000);
        checkbox1.click();
        log.info("Checkbox 1 is selected");
        Thread.sleep(1000);
        driver.close();
    }
    @Test(priority = 2)
    public void ContextClick () throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
        ExtentTest test = reports.createTest("ContextClick");
        driver.get("http://the-internet.herokuapp.com/");
        ContextMenuLink.click();
        test.info("After Click on ContextMenuLink");
        Actions action = new Actions(driver);  //dodajemy symulacje klikania prawym przy. myszki
        action.contextClick(HotSpotSquare).perform(); //symulacja prawego przycisku,
        test.info("After Click on Square");
        Alert alert = driver.switchTo().alert(); //przełączenie się na alert z strony, symulacja kliknięcia w "ok"
        alert.accept();
        Thread.sleep(2000);
        System.out.println("accept alert");
        driver.close();
    }
    @Test(priority = 3)
    public void dropDown() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
        ExtentTest test = reports.createTest("dropDown");
        driver.get("http://the-internet.herokuapp.com/");
        DropDownLink.click();
        Select select = new Select(DropdownList); //https://www.guru99.com/select-option-dropdown-selenium-webdriver.html
        select.selectByVisibleText("Option 1");
        Thread.sleep(2000);
        driver.close();
    }
    @Test(priority = 4)
    public void BasicAuth ()  {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
        ExtentTest test = reports.createTest("BasicAuth");

        //Nie chciało działać z alert.switchTo() wiec użyłem metody z
        //https://stackoverflow.com/questions/47173651/how-to-handle-browser-login-popup-using-selenium-java
        //wpisanie loginu i hasła przez url:
        //https://stackoverflow.com/questions/45345882/selenium-basic-authentication-via-url?noredirect=1&lq=1
        driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth");

        //asercja na podstawie
        //https://stackoverflow.com/questions/8498779/getpagesource-in-selenium-webdrivera-k-a-selenium2-using-java
        String expectedText = "Congratulations! You must have the proper credentials.";
        String pageSource = driver.getPageSource();
        boolean isTheTextPresent = driver.getPageSource().contains(expectedText);
        Assert.assertTrue(isTheTextPresent,"test massage text");
        System.out.println(true);
        driver.close();
    }
    @Test(priority = 5)
    public void Inputs () throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);
        ExtentTest test = reports.createTest("Inputs");
        driver.get("http://the-internet.herokuapp.com/");
        InputsLink.click();
        NumberInput.sendKeys("123456789");
        Thread.sleep(1000);
        driver.close();
    }

}
