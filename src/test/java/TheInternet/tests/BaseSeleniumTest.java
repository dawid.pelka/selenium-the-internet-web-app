package TheInternet.tests;

import TheInternet.helpers.DriverFactory;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalTime;

public class BaseSeleniumTest {

    protected ExtentHtmlReporter reporter;
    protected ExtentReports reports;
    protected WebDriver driver;


   @BeforeTest
    public void setUp() {
       String driverPath = "C:\\Users\\Wa-Dawid P\\IdeaProjects\\TheInternetWebApp\\src\\main\\resources\\executables\\drivers\\chromedriver.exe";
       System.setProperty("webdriver.chrome.driver", driverPath);
       driver = DriverFactory.getDriver();

   }
    @BeforeTest
    public  void setUpReporter() {
        reporter = new ExtentHtmlReporter("src//main//resources//reports//" + LocalTime.now().getNano() + ".html");
        reports = new ExtentReports();
        reports.attachReporter(reporter);
    }


    @FindBy(linkText = "Add/Remove Elements")
    public WebElement AddRemoveElementsButton;

    @FindBy(xpath = "//button[@onclick='addElement()']")
    public  WebElement AddElementButton;

    @FindBy(xpath = "//button[@class='added-manually']")
    public  WebElement DeleteButton;

    @FindBy(linkText = "Checkboxes")
    public WebElement checkboxesButton;

    @FindBy(linkText = "Context Menu")
    public WebElement ContextMenuLink;

    @FindBy(xpath = "//div[@id='hot-spot']")
    public WebElement HotSpotSquare;

    @FindBy(linkText = "Dropdown")
    public WebElement DropDownLink;

    @FindBy(xpath = "//select[@id='dropdown']")
    public WebElement DropdownList;

    @FindBy(linkText = "Inputs")
    public WebElement InputsLink;

    @FindBy(xpath = "//input[@type='number']")
    public WebElement NumberInput;

    public Logger log = Logger.getLogger(BaseSeleniumTest.class);


    @AfterTest
    public void tearDownReporter() {
        reporter.flush();
        reports.flush();
    }


}
